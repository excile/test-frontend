import React from "react";
import styles from "../styles/Alert.module.css";

interface Props {
    bgColor: string;
    color: string;
    value: string;
}

export default function Alert(props: Props) {
    return(
        <div className={styles.alert} style={{backgroundColor: props.bgColor, color: props.color}}>
            <p>{props.value}</p>
        </div>
    );
}