import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import styles from "../styles/Dashboard.module.css";
import {faCircleQuestion, faClock} from "@fortawesome/free-regular-svg-icons";

interface Props {
    name: string;
    secondary: string;
    reqPcnt: number;
    time: number;
    qCount: number;
}

const Test = (props: Props) => {
    return(
        <div className={"test"}>
            <div className={`column test-pad`}>
                <p>{props.name}</p>
                <p style={{color: "#ccc"}}>{props.secondary}</p>
            </div>
            <div className={`column ${styles['dotted-border']}`}>
                <p><FontAwesomeIcon icon={faClock} style={{width: 15}} /> {props.time}</p>
                <p><FontAwesomeIcon icon={faCircleQuestion} style={{width: 15}} /> {props.qCount}</p>
                <p>{props.reqPcnt}%</p>
            </div>
        </div>
    )
}

export default Test;