import React from "react";

interface Props {
    backgroundColor: string;
    primary: string;
    secondary: string;
}

export default function Result(props: Props) {
    return(
        <div className={"test"} style={{backgroundColor: props.backgroundColor}}>
            <div className={`column test-pad`}>
                <p>{props.primary}</p>
                <p style={{color: "#ccc"}}>{props.secondary}</p>
            </div>
        </div>
    )
}