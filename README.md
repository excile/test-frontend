Built with TypeScript, React, Next.JS

## Getting Started

Install dependencies:

```
npm install
# or 
yarn add
```

Start the development server:

```
npm run dev
# or
yarn dev
```

## Set up .env

NEXT_PUBLIC_API_URL - full link to the API. Example: http://localhost:5191/ (should have / in the end)
NEXT_PUBLIC_BASE_URL - base link to frontends own directory. Example: /test/ (should also have / in the end)
