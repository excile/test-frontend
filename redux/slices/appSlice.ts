import { createSlice, Slice } from "@reduxjs/toolkit";

const initialState = {
    test: "123"
};

export const appSlice: Slice = createSlice({
    name: "app",
    initialState,
    reducers: {
        test: (state, action) => {
            console.log(state.test);
        }
    }
});

export default appSlice.reducer;