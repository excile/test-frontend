export interface Result {
    test: string;
    score: number;
    scoreperc: string;
    status: number;
}

export interface Test {
    id: number;
    name: string;
    minutes: number;
    passingscore: number;
    qcount: number;
    assigned: string;
}

export interface Response {
    results: Result[];
    tests: Test[];
}