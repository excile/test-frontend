export interface JWT {
    username: string;
    jti: string;
    roles: string[];
    groups: string[];
    exp: string;
    iss: string;
    aud: string;
    avatar: string;
}