import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
// import Result from "../../components/Result";
import { Heading, IconButton, Button, Input, Select } from "@chakra-ui/react";
import { EditIcon, DeleteIcon, ArrowBackIcon, AddIcon } from "@chakra-ui/icons"
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
} from '@chakra-ui/react'

const Tests: NextPage = () => {
    return(
        <div className={"container"}>
            <Head>
                <title>test: mod</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <div className={"header"}>
                    <button onClick={() => window.location.href = "https://google.com/"}>Перейти на сайт</button>
                    <button onClick={() => window.location.href = "/mod"}>Результаты</button>
                    <button onClick={() => window.location.href = "/dashboard/"}>Дашборд</button>
                    <button>ADM</button>
                </div>
                <div className={"container"}>
                    <div className={"tests"}>
                        <div className={"inline gap10"}>
                            <IconButton aria-label='Вернуться' icon={<ArrowBackIcon />} />
                            <Heading size={"md"} className={"primary"}>Редактирование &quot;Вступительный тест  &quot;</Heading>
                        </div>
                        <Button leftIcon={<AddIcon />} colorScheme='gray' variant='solid' style={{marginBottom: 10}}>
                            Создать вопрос
                        </Button>
                        <TableContainer style={{border: '1px solid #2d3748', borderRadius: 10, color: "#ccc"}}>
                            <Table>
                                <Thead>
                                    <Tr>
                                        <Th>Вопрос</Th>
                                        <Th>Тип</Th>
                                        <Th>Действия</Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    <Tr>
                                        <Td>placeholder</Td>
                                        <Td>placeholder1</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                    <Tr>
                                        <Td>placeholder</Td>
                                        <Td>placeholder1</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                    <Tr>
                                        <Td>placeholder</Td>
                                        <Td>placeholder1</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                </Tbody>
                                <Tfoot>
                                    <Tr>
                                        <Td>placeholder</Td>
                                        <Td>placeholder1</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                </Tfoot>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default Tests;