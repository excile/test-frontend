import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
// import Result from "../../components/Result";
import { Heading, IconButton, Button, Input, Select } from "@chakra-ui/react";
import { EditIcon, DeleteIcon, AddIcon, CheckIcon } from "@chakra-ui/icons"
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
} from '@chakra-ui/react'

const Tests: NextPage = () => {
    return(
        <div className={"container"}>
            <Head>
                <title>test: mod</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <div className={"header"}>
                    <button onClick={() => window.location.href = "https://google.com/"}>Перейти на сайт</button>
                    <button onClick={() => window.location.href = "/mod"}>Результаты</button>
                    <button onClick={() => window.location.href = "/dashboard/"}>Дашборд</button>
                    <button>ADM</button>
                </div>
                <div className={"container"}>
                    <div className={"tests"}>
                        <Heading size={"md"} className={"primary"}>Назначить тест</Heading>
                        <Input placeholder='ID участника' style={{color: "#666"}} />
                        <div className={"inline"}>
                            <Select placeholder='Выберите тест' style={{color: "#666", width: "full"}}>
                            <option value='option1'>тест 1</option>
                            <option value='option2'>тест 2</option>
                            <option value='option3'>тест 3</option>
                            </Select>
                            <IconButton aria-label='Назначить тест' icon={<CheckIcon />} />
                        </div>
                        <Heading size={"md"} className={"primary"}>Тесты</Heading>
                        <Button leftIcon={<AddIcon />} colorScheme='gray' variant='solid' style={{marginBottom: 10}}>
                            Создать тест
                        </Button>
                        <TableContainer style={{border: '1px solid #2d3748', borderRadius: 10, color: "#ccc"}}>
                            <Table>
                                <Thead>
                                    <Tr>
                                        <Th>Название</Th>
                                        <Th>Количество вопросов</Th>
                                        <Th>Действия</Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    <Tr>
                                        <Td>Вступительный тест  </Td>
                                        <Td>666</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                    <Tr>
                                        <Td>Вступительный тест  </Td>
                                        <Td>666</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                    <Tr>
                                        <Td>Вступительный тест  </Td>
                                        <Td>666</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                </Tbody>
                                <Tfoot>
                                    <Tr>
                                        <Td>Вступительный тест  </Td>
                                        <Td>666</Td>
                                        <Td>
                                            <IconButton aria-label='Отредактировать тест' icon={<EditIcon />} style={{marginRight: 5}} />
                                            <IconButton aria-label='Удалить тест' icon={<DeleteIcon />} />
                                        </Td>
                                    </Tr>
                                </Tfoot>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default Tests;