import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
import Result from "../../components/Result";
import { Heading } from "@chakra-ui/react";

const mod: NextPage = () => {
    return(
        <div className={"container"}>
            <Head>
                <title>test: mod</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <div className={"header"}>
                    <button onClick={() => window.location.href = "https://google.com/"}>Перейти на сайт</button>
                    <button onClick={() => window.location.href = "/mod/tests"}>Управление тестами</button>
                    <button onClick={() => window.location.href = "/dashboard/"}>Дашборд</button>
                    <button>ADM</button>
                </div>
                <div className={"container"}>
                    <div className={"tests"}>
                        <Heading size={"md"} className={"primary title"}>Результаты</Heading>
                        <Result backgroundColor={"#FF6666"} primary={"excile! провалил Вступительный тест"} secondary={"12.02.2022 00:00, 2/69 2%"} />
                    </div>
                </div>
            </main>
        </div>
    );
}

export default mod;