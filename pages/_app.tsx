import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Provider } from "react-redux";
import { ChakraProvider, extendTheme, type ThemeConfig, ColorModeScript } from "@chakra-ui/react";
import { store } from "../redux/store";
import { mode } from "@chakra-ui/theme-tools";

const theme = extendTheme({
    styles: {
        global: (props: any) => ({
            body: {
                bg: mode("#fff","#222")(props),
            }
        })
    },
})


function MyApp({ Component, pageProps }: AppProps) {
  return (
      <ChakraProvider theme={theme}>
          <ColorModeScript initialColorMode={'dark'} />
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </ChakraProvider>
  );
}

export default MyApp
