import React from "react";
import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Image from 'next/image';
import Alert from "../components/Alert";
import axios from "axios";
import { decodeToken } from "react-jwt";
import type { JWT } from "../models/jwt";

const Home: NextPage = () => {
    const [status, setStatus] = React.useState({color: "#666", bgColor: "#252525", value: "Загрузка, подождите..", loading: true, fail: false});
    React.useEffect(() => {
      axios.post(`${process.env.NEXT_PUBLIC_API_URL}Login/Post`, {}, {withCredentials: true})
          .then(data => {
              let response = data.data;
              let decodedToken: JWT | null = decodeToken(response);
              localStorage.setItem("test-jwt", response);
              setStatus({
                  color: "#fff",
                  bgColor: "#2979ff",
                  value: `Добро пожаловать, ${decodedToken?.username}. Переходим в дашборд`,
                  loading: false,
                  fail: false
              });
              setTimeout(() => window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}dashboard`, 3000);
          })
          .catch(err => {
              switch(err.response.status) {
                  case 401: setStatus({color: "#fff", bgColor: "#ff4444", value: "Вы не авторизованы на сайте.", loading: false, fail: true}); break;
                  case 403: setStatus({color: "#fff", bgColor: "#ff4444", value: "У вас нету прав для входа.", loading: false, fail: true}); break;
                  default: setStatus({color: "#fff", bgColor: "#ff4444", value: `Произошла ошибка: ${err}`, loading: false, fail: true}); break;
              }
          })
    }, []);
    return (
      <>
          <Head>
              <title>test</title>
              <meta name="description" content="test" />
              <link rel="icon" href={`${process.env.NEXT_PUBLIC_BASE_URL}logo.png`} />
          </Head>

          <main className={styles.main}>
              <div className={`${styles['vh-center-column']} ${status.loading ? styles.flashing : ''} ${status.fail ? styles.shake : ''}`}>
                  <Image src={"/logo.png"} alt={"logo"} width={150} height={150} />
                  <Alert bgColor={status.bgColor} color={status.color} value={status.value} />
              </div>
          </main>
      </>
    )
}

export default Home
