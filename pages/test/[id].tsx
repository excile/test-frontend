import React from "react";
import Head from "next/head";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-regular-svg-icons";
import { Heading, Button, RadioGroup, Stack, Radio, Select } from "@chakra-ui/react";

export default function Test() {
    return (
        <>
            <Head>
                <title>Test</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <div className={"header"}>
                    <button onClick={() => window.location.href = "/dashboard/"}>Выйти</button>
                </div>
                <div className={"test-window"}>
                    <div className={"test-bar"}>
                        <p className={"test-name"}>
                            Вступительный тест
                        </p>
                        <div className={"question-header"}>
                            <Select className={"question-select"} bg={"#333"} placeholder='placeholder'>
                                <option value={"placeholder"}>placeholder</option>
                                <option value={"placeholder2"}>placeholder2</option>
                            </Select>
                        </div>
                    </div>
                    <div>
                        <Heading size={"md"} style={{color: '#ccc', padding: 15}}>
                         placeholder
                        </Heading>
                        <RadioGroup defaultValue='1' style={{paddingLeft: 15, paddingBottom: 15}}>
                            <Stack>
                                <Radio value='1'>
                                    25
                                </Radio>
                                <Radio value='2'>35</Radio>
                                <Radio value='3'>45</Radio>
                            </Stack>
                        </RadioGroup>
                        <div className={'flex-row'} style={{paddingRight: 15, paddingBottom: 15}}>
                            <Button colorScheme='green' variant='outline' style={{marginLeft: 'auto'}}>
                                Далее
                            </Button>
                        </div>
                    </div>
                    <div className={'flex-row test-timer'} style={{justifyContent: 'center', gap: 5}}>
                        <FontAwesomeIcon icon={faClock} style={{width: 16}} />
                        20:00
                    </div>
                </div>
            </main>
        </>
    );
}