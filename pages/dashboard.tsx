import type { NextPage } from "next";
import React from "react";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Dashboard.module.css";
import Result from "../components/Result";
import Test from "../components/Test";
import { decodeToken } from "react-jwt";
import type { JWT } from "../models/jwt";
import type { Response } from "../models/dashboard";
import { format } from "date-fns";
import { ru } from "date-fns/locale";
import ScaleLoader from "react-spinners/ScaleLoader";
import axios from "axios";
import { Heading } from "@chakra-ui/react";

const Dashboard: NextPage = () => {

    const [jwt, setJwt] = React.useState<JWT | null>(null);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [data, setData] = React.useState<Response | null>(null);

    React.useEffect(() => {
        let rawToken = localStorage.getItem("test-jwt");
        setJwt(decodeToken(rawToken!));

        axios.get(`${process.env.NEXT_PUBLIC_API_URL}Dashboard/Get`, {withCredentials: true, headers: {'Authorization': `Bearer ${rawToken}`}})
            .then(data => {
                setData(data.data);
                setLoading(false);
                console.log(data.data)
            })
            .catch(err => {
                setLoading(false);
                alert(`Ошибка ${err}.`);
                window.location.href = process.env.NEXT_PUBLIC_BASE_URL!;
            })
    }, []);

    return(
        <>
            {loading &&
                <div className={"vh-center"}><ScaleLoader color={"#aaa"} /></div>
            }
            {!loading &&
                <div className={"container"}>
                    <Head>
                        <title>Test</title>
                        <link rel="icon" href={`${process.env.NEXT_PUBLIC_BASE_URL}logo.png`} />
                    </Head>
                    <main>
                        <div className={"header"}>
                            <button onClick={() => window.location.href = "https://google.com/"}>Перейти на сайт</button>
                            <button onClick={() => window.location.href = "/complaints.png"}>Сообщить о проблеме</button>
                            <button onClick={() => window.location.href = "/mod/"}>MOD</button>
                            <button>ADM</button>
                        </div>
                        <div className={styles.welcome}>
                            <Image src={jwt?.avatar ?? `${process.env.NEXT_PUBLIC_BASE_URL}blank-pp.jpg`} alt={jwt?.username} width={50} height={50} style={{borderRadius: 100}} />
                            <span>
                        <p className={"primary"}>Привет, {jwt?.username ?? "N/A"} <span style={{color: "#555"}}>{` ${jwt?.roles.join(' ')}`}</span>
                        </p>
                        <p className={"secondary"}>{format(new Date(), "d MMMM, kk:mm", { locale: ru })}</p>
                    </span>
                        </div>
                        <div className={"container"}>
                            {data?.tests?.length! <= 0 &&
                                <div className={`tests`}>
                                    <Heading size={"md"} className={"primary title"}>Доступные тесты</Heading>
                                    <p className={`primary`}>
                                        У вас нету доступных тестов.
                                    </p>
                                </div>
                            }
                            {data?.tests?.length! > 0 &&
                                <div className={"tests"}>
                                    <Heading size={"md"} className={"primary title"}>Доступные тесты</Heading>
                                    {data?.tests.map(test => {
                                        return <Test key={test.id} name={test.name} secondary={`Назначен: ${format(new Date(test.assigned), "d MMMM, kk:mm", {locale: ru})}`} qCount={test.qcount} reqPcnt={test.passingscore} time={test.minutes} />;
                                    })}
                                </div>
                            }

                            {data?.results?.length! <= 0 &&
                                <div className={`tests`}>
                                    <Heading size={"md"} className={"primary title"}>Результаты</Heading>
                                    <p className={`primary`}>
                                        У вас нету результатов.
                                    </p>
                                </div>
                            }
                            {data?.results?.length! > 0 &&
                                <div className={"tests"}>
                                    <Heading size={"md"} className={"primary title"}>Результаты</Heading>
                                    {data?.results.map((result, idx) => {
                                        return <Result key={idx}
                                                       backgroundColor={Number(result.status) === 0 ? "#FF6666" : "green"}
                                                       primary={result.test}
                                                       secondary={`${result.status === 0 ? 'Провален' : 'Пройден'}, ${result.score} баллов, ${result.scoreperc}%`}/>;
                                    })}
                                </div>
                            }
                        </div>
                    </main>
                </div>
            }
        </>
    );
};

export default Dashboard;